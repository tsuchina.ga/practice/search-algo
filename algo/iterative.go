package algo

import (
    "strconv"
)

func IterativeSearch(tree *Node, limit int, goals []int) (result map[int]map[string]int) {
    result = make(map[int]map[string]int)
    step := 0
    maxMem := 0

    for i := 1;;i++ {
        depth := limit * i
        opens := []*Node{tree}
        var closes []*Node
        maxDepth := 0

        for {
            step += 1
            node := opens[0]
            opens = opens[1:]
            closes = append(closes, node)

            if node.Depth < depth {
                opens = append(node.Children, opens...)
            }

            if maxMem < len(opens) + len(closes) {
                maxMem = len(opens) + len(closes)
            }

            if maxDepth < node.Depth {
                maxDepth = node.Depth
            }

            for _, goal := range goals {
                if num, _ := strconv.Atoi(node.Name); num == goal {
                    if _, ok := result[num]; !ok {
                        result[num] = make(map[string]int)
                        result[num]["step"] = step
                        result[num]["memory"] = maxMem
                    }
                }
            }

            if len(opens) < 1 {
                break
            }
        }

        if maxDepth < depth {
            break
        }
    }

    return
}
