package algo

import (
    "fmt"
)

func CreateTree(branch, depth int) (tree *Node, nameNum int) {
    nameNum = 1
    name := fmt.Sprintf("%010d", nameNum)
    tree = &Node{Name: name, Depth: 0}

    for i := 1; i <= depth; i++ {
        nodes := getNodes(tree, i - 1)
        for _, node := range nodes {
            // log.Println(i, node.Name)
            for j := 0; j < branch; j++ {
                nameNum += 1
                name = fmt.Sprintf("%010d", nameNum)
                child := Node{Name: name, Depth: i}
                node.Children = append(node.Children, &child)
            }
        }
    }

    return
}

type Node struct {
    Name string
    Depth int
    Children []*Node
}

func getNodes(node *Node, depth int) (result []*Node) {
    if node.Depth != depth {
        for _, n := range node.Children {
            for _, nn := range getNodes(n, depth) {
                result = append(result, nn)
            }
        }
    } else {
        result = append(result, node)
    }

    return
}
