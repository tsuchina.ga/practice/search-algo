package algo

import (
    "strconv"
)

func BreadthSearch(tree *Node, goals []int) (result map[int]map[string]int) {
    result = make(map[int]map[string]int)
    opens := []*Node{tree}
    var closes []*Node
    step := 0
    maxMem := 0

    for {
        step += 1
        node := opens[0]
        opens = opens[1:]
        closes = append(closes, node)

        opens = append(opens, node.Children...)

        if maxMem < len(opens) + len(closes) {
            maxMem = len(opens) + len(closes)
        }

        for _, goal := range goals {
            if num, _ := strconv.Atoi(node.Name); num == goal {
                if _, ok := result[num]; !ok {
                    result[num] = make(map[string]int)
                    result[num]["step"] = step
                    result[num]["memory"] = maxMem
                }
            }
        }

        if len(opens) < 1 {
            break
        }
    }

    return
}
