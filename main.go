package main

import (
    "gitlab.com/tsuchina.ga/practice/search-algo/algo"
    "log"
    "os"
    "strconv"
)

func main() {

    branch := 2
    depth := 3

    if len(os.Args) >= 2 {
        b, err := strconv.Atoi(os.Args[1])
        if err == nil && b > branch {
            branch = b
        }
    }

    if len(os.Args) >= 3 {
        d, err := strconv.Atoi(os.Args[2])
        if err == nil && d > depth {
            depth = d
        }
    }

    tree, nodeNum := algo.CreateTree(branch, depth)

    middle := nodeNum+1
    for i := 0; i < depth/2; i++ {
        middle = middle/branch
    }

    goals := []int{middle - (middle - middle/branch)/2, nodeNum/branch + 1, nodeNum}
    log.Println(branch, depth, goals)

    result := make(map[int]map[string]int)
    // 幅優先探索
    // log.Println("start breadth")
    result = algo.BreadthSearch(tree, goals)
    log.Println(result)

    // 深さ優先探索
    // log.Println("start depth")
    result = algo.DepthSearch(tree, goals)
    log.Println(result)

    // 反復深化探索(1) = 幅優先探索
    // log.Println("iterative depth(1)")
    result = algo.IterativeSearch(tree, 1, goals)
    log.Println(result)

    // 反復深化探索(2)
    // log.Println("iterative depth(2)")
    result = algo.IterativeSearch(tree, 2, goals)
    log.Println(result)

    // 反復深化探索(3)
    // log.Println("iterative depth(3)")
    result = algo.IterativeSearch(tree, 3, goals)
    log.Println(result)
}
